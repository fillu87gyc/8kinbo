#include "pgmlib.h"
#include <stdio.h>
int Cij[3][3] = /* Cij[j][i] の順になることに注意 */
  {{0, -1, 0}, /* j=-1で i=-1,i=0,i=+1 と変化 */
   {-1, 5, -1}, /* j= 0で i=-1,i=0,i=+1 と変化 */
   {0, -1, 0}}; /* j=+1で i=-1,i=0,i=+1 と変化 */
int calc(int n, int x, int y);
void filtering(int n1, int n2);

int main(void)
{
  load_image(0, "blurry-moon.pgm");
  filtering(0, 1);
  save_image(1, "blurry-moon_4enh.pgm");
  return 0;
}

int calc(int n, int x, int y)
{
  int i, j, value = 0;
  for (j = 0; j < 3; j++)
    for (i = 0; i < 3; i++)
      value += Cij[i][j] * image[n][x + i - 1][y + j - 1];
  if (value > 255)
    value = 255;
  else if (value < 0)
    value = 0;
  return value;
}

void filtering(int n1, int n2)
{
  int x, y;
  int xsft, ysft;

  width[n2]  = width[n1];
  height[n2] = height[n1];
  for (y = 1; y < height[n1] - 1; y++)
  {
    for (x = 1; x < width[n1] - 1; x++)
    {
      image[n2][x][y] = calc(n1, x, y);
    }
  }
  for (y = 0; y < height[n2]; y++)
  {
    for (x = 0; x < width[n2]; x++)
    {
      xsft = 0;
      ysft = 0; /* 内側の画素は両方0で良い */
      if (x == 0)
        xsft = 1;
      else if (x == width[n2] - 1)
        xsft = -1;
      if (y == 0)
        ysft = 1;
      else if (y == height[n2] - 1)
        ysft = -1;
      if (xsft != 0 || ysft != 0) /* 外周部のみ処理 */
        image[n2][x][y] = image[n2][x + xsft][y + ysft];
    }
  }
}
